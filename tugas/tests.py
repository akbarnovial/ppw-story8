from django.test import TestCase, LiveServerTestCase
from django.test.client import Client
from django.apps import apps
from tugas.apps import TugasConfig
from django.http import HttpRequest
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from .views import index
import time


# Create your tests here.
class TestUnit(TestCase):

	def test_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code,200)

	def test_template_used(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'pages/index.html')

	def test_app(self):
		self.assertEqual(TugasConfig.name, 'tugas')
		self.assertEqual(apps.get_app_config('tugas').name, 'tugas')

	def test_title(self):
		response = Client().get('/')
		response_content = response.content.decode('utf-8')
		self.assertIn("Accordion", response_content)

	def test_resolve_function(self):
		found = resolve('/')
		self.assertEqual(found.func, index)

class FunctionalTestYogs(LiveServerTestCase):
	def setUp(self):
		super().setUp()
		chrome_options = webdriver.ChromeOptions()
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('--disable-gpu')
		chrome_options.add_argument('--disable-dev-shm-usage')

		self.browser = webdriver.Chrome(chrome_options=chrome_options, executable_path ='chromedriver')
		# self.browser = webdriver.Firefox()


	def tearDown(self):
		self.browser.quit()

	def test_title(self):
		self.browser.get(self.live_server_url)
		time.sleep(5)
		self.assertIn('Accordion',self.browser.title)

	def test_accordion_opens_closes(self):
		self.browser.get(self.live_server_url)

		button = self.browser.find_element_by_id('activity')
		button.click()
		time.sleep(5)
		self.assertEqual(self.browser.find_element_by_id('activity').get_attribute("aria-expanded"),"true")
		button.click()
		time.sleep(5)
		self.assertEqual(self.browser.find_element_by_id('activity').get_attribute("aria-expanded"), "false")

		button = self.browser.find_element_by_id('experience')
		button.click()
		time.sleep(5)
		self.assertEqual(self.browser.find_element_by_id('experience').get_attribute("aria-expanded"),"true")
		button.click()
		time.sleep(5)
		self.assertEqual(self.browser.find_element_by_id('experience').get_attribute("aria-expanded"), "false")

		button = self.browser.find_element_by_id('achievement')
		button.click()
		time.sleep(5)
		self.assertEqual(self.browser.find_element_by_id('achievement').get_attribute("aria-expanded"),"true")
		button.click()
		time.sleep(5)
		self.assertEqual(self.browser.find_element_by_id('achievement').get_attribute("aria-expanded"), "false")

		button = self.browser.find_element_by_id('profiecency')
		button.click()
		time.sleep(5)
		self.assertEqual(self.browser.find_element_by_id('profiecency').get_attribute("aria-expanded"),"true")
		button.click()
		time.sleep(5)
		self.assertEqual(self.browser.find_element_by_id('profiecency').get_attribute("aria-expanded"), "false")

	def test_accordion_up_down(self):
		self.browser.get(self.live_server_url)
		time.sleep(5)
		self.browser.find_element_by_id('upButton2').click()
		card = self.browser.find_element_by_xpath("//div[@id='accordionExample275']/div[@class='card z-depth-0 bordered'][1]")
		self.assertIn("acc2", card.get_attribute("id"))
		time.sleep(5)
		self.browser.find_element_by_id('downButton2').click()
		card = self.browser.find_element_by_xpath("//div[@id='accordionExample275']/div[@class='card z-depth-0 bordered'][2]")
		self.assertIn("acc2", card.get_attribute("id"))

	# def test_change_theme(self):
	# 	self.driver.get(self.live_server_url)
	# 	self.driver.find_by_element_by_id("changetheme").click()
	# 	current = self.driver.find_element_byxpath("//div]@id='accordionExample275']")
	# 	self.assertIn("bg-dark", current.get_attribute("class"))
